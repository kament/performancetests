USE [master]
GO
/****** Object:  Database [TestPerformance]    Script Date: 01/24/2017 21:06:23 ******/
CREATE DATABASE [TestPerformance]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'NewPerformance', FILENAME = N'C:\Users\Kamen\NewPerformance.mdf' , SIZE = 1974272KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'NewPerformance_log', FILENAME = N'C:\Users\Kamen\NewPerformance_log.ldf' , SIZE = 2629632KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [TestPerformance].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [TestPerformance] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [TestPerformance] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [TestPerformance] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [TestPerformance] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [TestPerformance] SET ARITHABORT OFF 
GO
ALTER DATABASE [TestPerformance] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [TestPerformance] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [TestPerformance] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [TestPerformance] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [TestPerformance] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [TestPerformance] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [TestPerformance] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [TestPerformance] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [TestPerformance] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [TestPerformance] SET  DISABLE_BROKER 
GO
ALTER DATABASE [TestPerformance] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [TestPerformance] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [TestPerformance] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [TestPerformance] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [TestPerformance] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [TestPerformance] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [TestPerformance] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [TestPerformance] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [TestPerformance] SET  MULTI_USER 
GO
ALTER DATABASE [TestPerformance] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [TestPerformance] SET DB_CHAINING OFF 
GO
ALTER DATABASE [TestPerformance] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [TestPerformance] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [TestPerformance] SET DELAYED_DURABILITY = DISABLED 
GO
USE [TestPerformance]
GO
/****** Object:  Table [dbo].[Parameters]    Script Date: 01/24/2017 21:06:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Parameters](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Parameter] [nvarchar](250) NOT NULL,
	[Value] [nvarchar](500) NULL,
	[AccountId] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [Parameter_AccountId_IX]    Script Date: 01/24/2017 21:06:23 ******/
CREATE NONCLUSTERED INDEX [Parameter_AccountId_IX] ON [dbo].[Parameters]
(
	[Parameter] ASC,
	[AccountId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
USE [master]
GO
ALTER DATABASE [TestPerformance] SET  READ_WRITE 
GO
