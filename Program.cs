﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using EntityFramework.BulkInsert.Extensions;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        { 
        
            //Create10MilionRecords();

            using (var ctx = new ParametersDbContext())
            {
                var asd = ctx.Parameters.FirstOrDefault(x => x.Id == 2876);

                var startDtm = DateTime.Now;
                var @params = ctx.Parameters.Select(x => x.AccountId == 99999999 && x.Parameter1 == "MerchantAccount").FirstOrDefault();
                var endDate = DateTime.Now;
                var res = (endDate - startDtm);
                Console.WriteLine($"Result of Select query by index mil: {res.TotalMinutes} sec: {res.TotalSeconds}");

                startDtm = DateTime.Now;
                ctx.Parameters.Add(new Parameter(Parameters[1], RandomValue(), 11000000));
                endDate = DateTime.Now;
                res = endDate - startDtm;
                Console.WriteLine($"Result of Insert query mil:{res.TotalMilliseconds} sec:{res.TotalSeconds}");

                startDtm = DateTime.Now;
                var test = ctx.Parameters.Select(x => x.AccountId == 1234).ToList();
                endDate = DateTime.Now;
                res = endDate - startDtm;
                Console.WriteLine($"Result of Select query by AccountStatementId mil:{res.TotalMilliseconds} sec:{res.TotalSeconds}");
            }
        }

        static void Create10MilionRecords()
        {
            var accountId = 999;
            for (int i = 0; i < 1000; i++)
            {
                var entities = new List<Parameter>(10000);
                for (int j = 0; j < 1000; j++)
                {
                    for (int p = 1; p < 11; p++)
                    {
                        entities.Add(new Parameter(Parameters[p], RandomValue(), accountId));
                    }

                    accountId++;
                }

                Save(entities);

                Console.WriteLine($"10 000 elements added - {i}");
            }



        }

        static string RandomValue()
        {
            return Guid.NewGuid().ToString();
        }

        static IDictionary<int, string> Parameters = new Dictionary<int, string>
        {
            {1,"EmpOrderId" },
            {2,"WirecardTransactionID" },
            {3,"OrderReference" },
            {4,"AccountNumber" },
            {5,"MerchantAccount" },
            {6,"CardNUmber" },
            {7,"NewOrder" },
            {8,"SomethingRandom" },
            {9,"FingerPrint" },
            {10,"ThisIsValue" }
        };


        private static void Save(List<Parameter> entities)
        {
            using (var ctx = new ParametersDbContext())
            {
                using (var transactionScope = new TransactionScope())
                {
                    // some stuff in dbcontext

                    ctx.BulkInsert(entities);

                    ctx.SaveChanges();
                    transactionScope.Complete();
                }
            }
        }
    }
}
