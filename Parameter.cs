namespace ConsoleApplication1
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Parameter
    {
        public Parameter()
        {

        }
        public Parameter(string param, string value, int accountId)
        {
            Parameter1 = param;
            Value = value;
            AccountId = accountId;
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Column("Parameter")]
        [Required]
        [StringLength(250)]
        public string Parameter1 { get; set; }

        [StringLength(500)]
        public string Value { get; set; }

        public long AccountId { get; set; }
    }
}
